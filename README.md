# Laptop Setup

1. Fully upgrade system `sudo apt-get update && sudo apt-get -f install && sudo apt-get full-upgrade`
2. Install `git` if not available: `sudo apt-get install git`
3. Generate SSH key with `ssh-keygen` or import from backup. Add to agent and gitlab
4. Install ansible: `sudo apt-get install ansible`
5. Clone this project: `git clone --recursive git@gitlab.com:tume/ubuntu-config.git`
6. Cd into cloned directory: `cd ubuntu-config`
7. Install Ansible dependencies:
```
ansible-galaxy collection install community.general
ansible-galaxy install jaredhocutt.gnome_extensions
ansible-galaxy install fubarhouse.rust
ansible-galaxy install nickpack.android_sdk
```
8. Inspect variables in `vars/main.yml`: do any of the URLs etc. need to be udpated?
9. Run ansible `ansible-playbook playbook.yml --inventory 127.0.0.1, -c local -K`
10. Restart Gnome shell (typically Alt+F2, then input 'r' and press enter).
11. Setup IntelliJ plugins?
