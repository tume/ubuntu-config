JAVA_HOME=/opt/jdk/jdk-8
JAVA_OPTS="$JAVA_OPTS -Dabakus.home=/opt/abakus-home/symlink"
JAVA_OPTS="$JAVA_OPTS -Dorg.apache.el.parser.SKIP_IDENTIFIER_CHECK=true"
JAVA_OPTS="$JAVA_OPTS -Xdebug -Xrunjdwp:transport=dt_socket,address=8788,server=y,suspend=n"
JAVA_OPTS="$JAVA_OPTS -XX:MaxPermSize=512m -Xmx3072m"